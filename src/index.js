const form_submit = document.getElementById("form_submit");
var url = new URL(window.location.href);
console.log("url", url);
var userId = url.searchParams.get("userId"); 
console.log(userId);
var socket = io.connect('ws://192.168.1.10:3000');
console.log('socket', socket);
socket.on("connect_error", (err) => {
  console.log(`connect_error due to ${err.message}`);
});
function showToast() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  }
form_submit.addEventListener("click", (e) => {
    e.preventDefault();
    var msg = document.getElementById("msg").value;
    socket.emit('customMsg', {message: msg, userId: userId})
    showToast();
    document.getElementById("msg").value = '';
});
